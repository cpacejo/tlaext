---- MODULE Maplets ----

(*
Operators for working with maps which are exposed both by Maps and TLC.
*)

\* A singleton map (maplet).
k :> v == [ kk \in { k } |-> v ]

\* Map overlay; LHS-preferred.
a @@ b == [ k \in (DOMAIN a) \union (DOMAIN b) |-> IF k \in DOMAIN a THEN a[k] ELSE b[k] ]

====
