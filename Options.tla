---- MODULE Options ----

(*
A standard option type, implemented as a set of zero or one members.
*)

Some(x) == { x }
None == { }

HasOpt(o) == \E x \in o: TRUE
GetOpt(o) == CHOOSE x \in o: TRUE

\* Option overlay; LHS-preferred.
a ++ b == IF HasOpt(a) THEN a ELSE b

TransformOpt(o, F(_)) == { F(x): x \in o }

====
