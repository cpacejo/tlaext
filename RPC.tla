---- MODULE RPC ----

CONSTANTS Cookie, nil
ASSUME nil \notin Cookie

VARIABLE requests

LOCAL INSTANCE MapsTLCCompat
LOCAL INSTANCE TLC

vars == <<requests>>

----

Invariant ==
  DOMAIN requests \subseteq (Cookie \union { nil })

Symmetry == Permutations(Cookie)

Constraint ==
  DOMAIN requests \subseteq Cookie

----

FreeCookies ==
  LET free == Cookie \ DOMAIN requests IN
  IF free = { } THEN { nil } ELSE free


LOCAL SendRequestAux(cookie, request, ignore_reply) ==
  /\ Assert(cookie \notin DOMAIN requests, "reused cookie")
  /\ Assert(request /= nil, "nil request")
  /\ requests' = (cookie :> [
      request |-> request,
      request_received |-> FALSE,
      reply |-> nil,
      reply_sent |-> FALSE,
      reply_received |-> ignore_reply
    ]) @@ requests

SendRequest(cookie, request) == SendRequestAux(cookie, request, FALSE)

SendRequestIgnoreReply(cookie, request) == SendRequestAux(cookie, request, TRUE)


IgnoreReply(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].reply_received => requests[cookie].reply_sent, "double ignore")
  /\ IF requests[cookie].reply_sent THEN
      requests' = requests -- cookie
    ELSE
      requests' = [ requests EXCEPT ![cookie].reply_received = TRUE ]


LOCAL SentRequests == { cookie \in DOMAIN requests: ~requests[cookie].request_received }


LOCAL ReceiveRequest(cookie) ==
  /\ Assert(cookie /= nil, "unconstrained cookies")
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(~requests[cookie].request_received, "double request receive")
  /\ requests' = [ requests EXCEPT ![cookie].request_received = TRUE ]


ReceivedRequests ==
  { cookie \in DOMAIN requests:
    requests[cookie].request_received /\ requests[cookie].request /= nil }


Request(cookie) ==
  CASE
    /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
    /\ Assert(requests[cookie].request_received, "read unreceived request")
    /\ Assert(requests[cookie].request /= nil, "read acked request")
  -> requests[cookie].request


\* optional, can be used to reduce state space
AckRequest(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].request_received, "acked unreceived request")
  /\ Assert(requests[cookie].request /= nil, "double-acked request")
  /\ requests' = [ requests EXCEPT ![cookie].request = nil ]


NoReply(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].request_received, "replied unreceived request")
  /\ Assert(~requests[cookie].reply_sent, "double reply")
  /\ Assert(~requests[cookie].reply_received, "unsent reply ignored")
  /\ requests' = requests -- cookie


SendReply(cookie, reply) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].request_received, "replied unreceived request")
  /\ Assert(~requests[cookie].reply_sent, "double reply")
  /\ Assert(reply /= nil, "nil reply")
  /\ IF requests[cookie].reply_received THEN
      requests' = requests -- cookie
    ELSE
      requests' = [ requests EXCEPT
        ![cookie].request = nil,
        ![cookie].reply = reply,
        ![cookie].reply_sent = TRUE
      ]


LOCAL SentReplies ==
  { cookie \in DOMAIN requests:
    requests[cookie].reply_sent /\ ~requests[cookie].reply_received }


LOCAL ReceiveReply(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].reply_sent, "received unsent reply")
  /\ Assert(~requests[cookie].reply_received, "double reply receive")
  /\ requests' = [ requests EXCEPT ![cookie].reply_received = TRUE ]


ReplyReceived(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ requests[cookie].reply_received


Reply(cookie) ==
  CASE
    /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
    /\ Assert(requests[cookie].reply_received, "read unreceived reply")
  -> requests[cookie].reply


AckReply(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].reply_received, "acked unreceived reply")
  /\ requests' = requests -- cookie

----

\* Ignore all replies for the indicated cookies.  Intended
\* to mimic the death of a client which had sent those requests.
IgnoreReplies(cookies) ==
  /\ Assert(cookies \subseteq DOMAIN requests, "unknown cookie")
  /\ Assert(\A cookie \in cookies:
      requests[cookie].reply_received => requests[cookie].reply_sent, "double ignore")
  /\ requests' = [
      cookie \in {
        cookie \in DOMAIN requests: cookie \in cookies =>
        ~requests[cookie].reply_sent
      }
    |->
      [ requests[cookie] EXCEPT !.reply_received = @ \/ cookie \in cookies ]
    ]


\* Send all replies indicated by the given function.  A reply of `nil`
\* corresponds to no reply.  Intended to mimic the death of a server
\* which was handling those requests.
SendReplies(replies) ==
  /\ Assert(DOMAIN replies \subseteq DOMAIN requests, "unknown cookie")
  /\ Assert(\A cookie \in DOMAIN replies: requests[cookie].request_received, "replied unreceived request")
  /\ Assert(\A cookie \in DOMAIN replies: ~requests[cookie].reply_sent, "double reply")
  /\ Assert(\A cookie \in DOMAIN replies:
      replies[cookie] = nil => ~requests[cookie].reply_received, "unsent reply ignored")
  /\ requests' = [
      cookie \in { cookie \in DOMAIN requests:
        cookie \in DOMAIN replies =>
        ~requests[cookie].reply_received /\ replies[cookie] /= nil
      }
    |->
      IF cookie \in DOMAIN replies THEN
        [ requests[cookie] EXCEPT
          !.request = nil,
          !.reply = replies[cookie],
          !.reply_sent = TRUE ]
      ELSE
        requests[cookie]
    ]

----

Init == requests = EmptyMap

Next ==
  \/ \E cookie \in SentRequests: ReceiveRequest(cookie)
  \/ \E cookie \in SentReplies: ReceiveReply(cookie)

Temporal ==
  /\ \A cookie \in Cookie:
    /\ WF_vars(cookie \in SentRequests /\ ReceiveRequest(cookie))
    /\ WF_vars(cookie \in SentReplies /\ ReceiveReply(cookie))

====
