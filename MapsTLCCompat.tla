---- MODULE MapsTLCCompat ----

(*
A standard map type, implemented as a function whose domain is the set of
keys currently in the map, and whose range is the associated values.  The
API is modeled after the standard Bags module, and is compatible with the function
composition operators from the TLC module.
*)

LOCAL INSTANCE FiniteSets

IsAMap(V, m) == m \in [ DOMAIN m -> V ]

----

\* Transform a map into a relation (set of key-value pairs).
MapToSet(m) == { <<k, m[k]>>: k \in DOMAIN m }

\* Transform a relation into a map.
SetToMap(s) == [ k \in { kv[1]: kv \in s } |-> (CHOOSE kv \in s: kv[1] = k)[2] ]

----

\* The empty map.
EmptyMap == [ k \in { } |-> { } ]

\* Map cardinality.
MapCardinality(m) == Cardinality(DOMAIN m)

\* Remove keys not matching `Test(key)` from map.
SelectMap(m, Test(_)) ==
  [ k \in { k \in DOMAIN m: Test(k) } |-> m[k] ]

\* Remove the given key from map.
m -- k ==
  [ kk \in DOMAIN m \ { k } |-> m[kk] ]

====
