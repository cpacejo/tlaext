---- MODULE Misc ----

(*
Miscellaneous helpful operators.  Stateless.
*)

LOCAL INSTANCE Naturals
LOCAL INSTANCE Sequences

FoldFunction(_(+)_, zero, f) ==
  LET reduce_over[d \in SUBSET DOMAIN f] ==
    IF \E x \in d: TRUE THEN
      f[CHOOSE x \in d: TRUE] (+) reduce_over[d \ { CHOOSE x \in d: TRUE }]
    ELSE zero
  IN reduce_over[DOMAIN f]

Sum(f) == FoldFunction(+, 0, f)

IsInjective(f) ==
  \A x, y \in DOMAIN f: f[x] = f[y] => x = y

SetToSeq(S) ==
  LET aux[q \in Seq(S), s \in SUBSET S] ==
    IF \E x \in s: TRUE THEN
      LET x == CHOOSE x \in s: TRUE IN
      aux[Append(q, x), s \ { x }]
    ELSE q
  IN aux[<<>>, S]

====
