---- MODULE Misc_types ----

INSTANCE Misc

LOCAL INSTANCE FiniteSets
LOCAL INSTANCE Reals
LOCAL INSTANCE TLAPS


THEOREM FoldFunctionType ==
  ASSUME
    NEW T,
    NEW f, f \in [ DOMAIN f -> T ], IsFiniteSet(DOMAIN f),
    NEW _(+)_, ASSUME NEW x \in T, NEW y \in T PROVE x (+) y \in T,
    NEW zero \in T
  PROVE
    FoldFunction((+), zero, f) \in T


THEOREM
  ASSUME
    NEW f, f \in [ DOMAIN f -> Nat ], IsFiniteSet(DOMAIN f)
  PROVE
    Sum(f) \in Nat

THEOREM
  ASSUME
    NEW f, f \in [ DOMAIN f -> Int ], IsFiniteSet(DOMAIN f)
  PROVE
    Sum(f) \in Int

THEOREM
  ASSUME
    NEW f, f \in [ DOMAIN f -> Real ], IsFiniteSet(DOMAIN f)
  PROVE
    Sum(f) \in Real


THEOREM
  ASSUME
    NEW T,
    NEW f, f \in [ DOMAIN f -> T ]
  PROVE
    IsInjective(f) \in BOOLEAN
PROOF
  BY DEF IsInjective

====
