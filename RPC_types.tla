---- MODULE RPC ----

CONSTANTS Cookie, nil
ASSUME nil \notin Cookie

VARIABLE requests

LOCAL INSTANCE MapsTLCCompat
LOCAL INSTANCE TLC
LOCAL INSTANCE Maps_types

vars == <<requests>>

----

\* FIXME: need to make dependent type, to be able to prove type of Reply
LOCAL StateT(ReqT, RepT) ==
  [
    request: ReqT \union { nil },
    request_received: BOOLEAN,
    reply: RepT \union { nil },
    reply_sent: BOOLEAN,
    reply_received: BOOLEAN
  ]

LOCAL TypeInvariant(ReqT, RepT) ==
  DOMAIN requests \subseteq (Cookie \union { nil }) /\ IsAMap(StateT(ReqT, RepT), requests)

USE DEFS TypeInvariant, StateT, IsAMap

----

Invariant ==
  DOMAIN requests \subseteq (Cookie \union { nil })

Symmetry == Permutations(Cookie)

Constraint ==
  DOMAIN requests \subseteq Cookie

USE DEF Constraint

----

FreeCookies ==
  LET free == Cookie \ DOMAIN requests IN
  IF free = { } THEN { nil } ELSE free

THEOREM FreeCookiesType ==
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    TypeInvariant(ReqT, RepT)
  PROVE
    /\ \E cookie \in FreeCookies: TRUE
    /\ FreeCookies \subseteq Cookie \/ FreeCookies = { nil }
PROOF
  BY DEF FreeCookies


LOCAL SendRequestAux(cookie, request, ignore_reply) ==
  /\ Assert(cookie \notin DOMAIN requests, "reused cookie")
  /\ Assert(request /= nil, "nil request")
  /\ requests' = (cookie :> [
      request |-> request,
      request_received |-> FALSE,
      reply |-> nil,
      reply_sent |-> FALSE,
      reply_received |-> ignore_reply
    ]) @@ requests


SendRequest(cookie, request) == SendRequestAux(cookie, request, FALSE)

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW cookie \in Cookie,
    NEW request \in ReqT
  PROVE
    SendRequest(cookie, request) \in BOOLEAN
PROOF
  BY DEFS SendRequest, SendRequestAux


SendRequestIgnoreReply(cookie, request) == SendRequestAux(cookie, request, TRUE)

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW cookie \in Cookie,
    NEW request \in ReqT
  PROVE
    SendRequestIgnoreReply(cookie, request) \in BOOLEAN
PROOF
  BY DEFS SendRequestIgnoreReply, SendRequestAux


IgnoreReply(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].reply_received => requests[cookie].reply_sent, "double ignore")
  /\ IF requests[cookie].reply_sent THEN
      requests' = requests -- cookie
    ELSE
      requests' = [ requests EXCEPT ![cookie].reply_received = TRUE ]

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW cookie \in Cookie
  PROVE
    IgnoreReply(cookie) \in BOOLEAN
PROOF
  BY DEFS IgnoreReply, --


LOCAL SentRequests == { cookie \in DOMAIN requests: ~requests[cookie].request_received }


LOCAL ReceiveRequest(cookie) ==
  /\ Assert(cookie /= nil, "unconstrained cookies")
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(~requests[cookie].request_received, "double request receive")
  /\ requests' = [ requests EXCEPT ![cookie].request_received = TRUE ]


ReceivedRequests ==
  { cookie \in DOMAIN requests:
    requests[cookie].request_received /\ requests[cookie].request /= nil }

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT)
  PROVE
    ReceivedRequests \subseteq Cookie
PROOF
  BY DEF ReceivedRequests


Request(cookie) ==
  CASE
    /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
    /\ Assert(requests[cookie].request_received, "read unreceived request")
    /\ Assert(requests[cookie].request /= nil, "read acked request")
  -> requests[cookie].request

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW cookie \in Cookie
  PROVE
    Request(cookie) \in ReqT


\* optional, can be used to reduce state space
AckRequest(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].request_received, "acked unreceived request")
  /\ Assert(requests[cookie].request /= nil, "double-acked request")
  /\ requests' = [ requests EXCEPT ![cookie].request = nil ]

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW cookie \in Cookie
  PROVE
    AckRequest(cookie) \in BOOLEAN
PROOF
  BY DEF AckRequest


NoReply(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].request_received, "replied unreceived request")
  /\ Assert(~requests[cookie].reply_sent, "double reply")
  /\ Assert(~requests[cookie].reply_received, "unsent reply ignored")
  /\ requests' = requests -- cookie

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW cookie \in Cookie
  PROVE
    NoReply(cookie) \in BOOLEAN
PROOF
  BY DEF NoReply


SendReply(cookie, reply) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].request_received, "replied unreceived request")
  /\ Assert(~requests[cookie].reply_sent, "double reply")
  /\ Assert(reply /= nil, "nil reply")
  /\ IF requests[cookie].reply_received THEN
      requests' = requests -- cookie
    ELSE
      requests' = [ requests EXCEPT
        ![cookie].request = nil,
        ![cookie].reply = reply,
        ![cookie].reply_sent = TRUE
      ]

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW cookie \in Cookie,
    NEW reply \in RepT
  PROVE
    SendReply(cookie, reply) \in BOOLEAN
PROOF
  BY DEF SendReply


LOCAL SentReplies ==
  { cookie \in DOMAIN requests:
    requests[cookie].reply_sent /\ ~requests[cookie].reply_received }


LOCAL ReceiveReply(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].reply_sent, "received unsent reply")
  /\ Assert(~requests[cookie].reply_received, "double reply receive")
  /\ requests' = [ requests EXCEPT ![cookie].reply_received = TRUE ]


ReplyReceived(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ requests[cookie].reply_received

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW cookie \in Cookie
  PROVE
    ReplyReceived(cookie) \in BOOLEAN
PROOF
  BY DEF ReplyReceived


Reply(cookie) ==
  CASE
    /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
    /\ Assert(requests[cookie].reply_received, "read unreceived reply")
  -> requests[cookie].reply

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW cookie \in Cookie,
    ReplyReceived(cookie)
  PROVE
    Reply(cookie) \in RepT
PROOF
  OMITTED  \* TODO: see above comment on StateT


AckReply(cookie) ==
  /\ Assert(cookie \in DOMAIN requests, "unknown cookie")
  /\ Assert(requests[cookie].reply_received, "acked unreceived reply")
  /\ requests' = requests -- cookie

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW cookie \in Cookie,
    ReplyReceived(cookie)
  PROVE
    AckReply(cookie) \in BOOLEAN
PROOF
  BY DEF AckReply

----

\* Ignore all replies for the indicated cookies.  Intended
\* to mimic the death of a client which had sent those requests.
IgnoreReplies(cookies) ==
  /\ Assert(cookies \subseteq DOMAIN requests, "unknown cookie")
  /\ Assert(\A cookie \in cookies:
      requests[cookie].reply_received => requests[cookie].reply_sent, "double ignore")
  /\ requests' = [
      cookie \in {
        cookie \in DOMAIN requests: cookie \in cookies =>
        ~requests[cookie].reply_sent
      }
    |->
      [ requests[cookie] EXCEPT !.reply_received = @ \/ cookie \in cookies ]
    ]

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW cookies, cookies \subseteq Cookie
  PROVE
    IgnoreReplies(cookies) \in BOOLEAN
PROOF
  BY DEFS IgnoreReplies


\* Send all replies indicated by the given function.  A reply of `nil`
\* corresponds to no reply.  Intended to mimic the death of a server
\* which was handling those requests.
SendReplies(replies) ==
  /\ Assert(DOMAIN replies \subseteq DOMAIN requests, "unknown cookie")
  /\ Assert(\A cookie \in DOMAIN replies: requests[cookie].request_received, "replied unreceived request")
  /\ Assert(\A cookie \in DOMAIN replies: ~requests[cookie].reply_sent, "double reply")
  /\ Assert(\A cookie \in DOMAIN replies:
      replies[cookie] = nil => ~requests[cookie].reply_received, "unsent reply ignored")
  /\ requests' = [
      cookie \in { cookie \in DOMAIN requests:
        cookie \in DOMAIN replies =>
        ~requests[cookie].reply_received /\ replies[cookie] /= nil
      }
    |->
      IF cookie \in DOMAIN replies THEN
        [ requests[cookie] EXCEPT
          !.request = nil,
          !.reply = replies[cookie],
          !.reply_sent = TRUE ]
      ELSE
        requests[cookie]
    ]

THEOREM
  ASSUME
    NEW ReqT, NEW RepT, nil \notin ReqT, nil \notin RepT,
    Constraint, TypeInvariant(ReqT, RepT),
    NEW replies \in [ Cookie |-> RepT \union { nil } ]
  PROVE
    SendReplies(replies) \in BOOLEAN
PROOF
  BY DEF SendReplies

----

Init == requests = EmptyMap

Next ==
  \/ \E cookie \in SentRequests: ReceiveRequest(cookie)
  \/ \E cookie \in SentReplies: ReceiveReply(cookie)

Temporal ==
  /\ \A cookie \in Cookie:
    /\ WF_vars(cookie \in SentRequests /\ ReceiveRequest(cookie))
    /\ WF_vars(cookie \in SentReplies /\ ReceiveReply(cookie))

====
