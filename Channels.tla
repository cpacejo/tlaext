---- MODULE Channels ----

LOCAL INSTANCE Naturals
LOCAL INSTANCE Sequences
LOCAL INSTANCE TLC

CONSTANT Channel
CONSTANT ChannelType(_)
CONSTANT ChannelZero(_)

CONSTANT nil

VARIABLE channels

----

Init ==
  channels = [ ch \in Channel |->
      [
        data |-> << >>,
        size |-> 0,
        open |-> FALSE,
        waiters |-> 0
      ]
    ]

----

MakeChannel(ch, size) ==
  /\ Assert(ch /= nil, "attempt to make nil channel")
  /\ Assert(channels[ch].waiters = 0, "attempt to reuse channel with waiters")
  /\ channels' = [ channels EXCEPT ![ch] = [
        data |-> << >>,
        size |-> size,
        open |-> TRUE,
        waiters |-> 0
      ] ]

NewChannel(ch) == MakeChannel(ch, 0)

\* not necessary, can be used to reduce state space
FreeChannel(ch) ==
  /\ Assert(ch /= nil, "attempt to free nil channel")
  /\ Assert(channels[ch].waiters = 0, "attempt to free channel with waiters")
  /\ channels' = [ channels EXCEPT ![ch] = [
        data |-> << >>,
        size |-> 0,
        open |-> FALSE,
        waiters |-> 0
      ] ]

(* Send is only valid if channel is open or nil. *)
ChannelSendValid(ch) == ch /= nil => channels[ch].open

ChannelSend(ch, data) ==
  /\ ch /= nil  \* Send to nil always blocks
  /\ Assert(ChannelSendValid(ch), "invalid send")
  /\ Len(channels[ch].data) < channels[ch].size + channels[ch].waiters
  /\ channels' = [ channels EXCEPT ![ch].data = Append(@, data) ]

(* Close is only valid if channel is open and not nil. *)
ChannelCloseValid(ch) == ch /= nil /\ channels[ch].open

ChannelClose(ch) ==
  /\ Assert(ChannelCloseValid(ch), "invalid send")
  /\ channels' = [ channels EXCEPT
      ![ch].open = FALSE,
      ![ch].size = IF channels[ch].data = << >> THEN 0 ELSE @  \* reduce state space
    ]

(* Shortcut for wait-receive which can be used with channels of non-zero size *)
ChannelTryReceive(ch) ==
  /\ ch /= nil  \* Receive from nil always blocks
  /\ Assert(channels[ch].size > 0, "try-receive on size 0 channel")
  \* We must hold off receiving if a send-waiter exchange is in progress
  /\ Len(channels[ch].data) <= channels[ch].size
  /\ IF channels[ch].data /= << >>
      channels' = [ channels EXCEPT ![ch].data = Tail(@) ]
    ELSE
      /\ ~channels[ch].open
      /\ channels' = [ channels EXCEPT ![ch].size = 0 (* reduce state space *) ]

ChannelWait(ch) ==
  IF ch /= nil THEN
    \* We must hold off waiting if a send-waiter exchange is in progress
    /\ Len(channels[ch].data) <= channels[ch].size
    /\ channels' = [ channels EXCEPT ![ch].waiters = @ + 1 ]
  ELSE
    UNCHANGED channels

ChannelReceive(ch) ==
  /\ ch /= nil  \* Receive from nil always blocks
  /\ Assert(channels[ch].waiters > 0, "receive from unwaited channel")
  /\ IF channels[ch].data /= << >> THEN
      channels' = [ channels EXCEPT ![ch].waiters = @ - 1, ![ch].data = Tail(@) ]
    ELSE
      /\ ~channels[ch].open
      /\ channels' = [ channels EXCEPT ![ch].waiters = @ - 1, ![ch].size = 0 (* reduce state space *) ]

\* UGGGGGGHHHHHHHH
\* TODO:
\* multi-select....
\* but we need to change how zero-length channels work!
\* since multi-select only performs ONE transfer!
\* I guess we need a concept of PID...
\* :(

ChannelData(ch) ==
  CASE
    Assert(ch /= nil /\ (channels[ch].open => channels[ch].data /= << >>), "read from empty channel")
  -> IF channels[ch].open THEN Head(channels[ch].data) ELSE ChannelZero(ch)

====
