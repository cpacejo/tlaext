---- MODULE Maps_types ----

INSTANCE Maps

LOCAL INSTANCE FiniteSets
LOCAL INSTANCE Naturals


THEOREM
  ASSUME
    NEW V, NEW m
  PROVE
    IsAMap(V, m) \in BOOLEAN
PROOF
  BY DEF IsAMap

USE DEF IsAMap

----

THEOREM
  ASSUME
    NEW K, NEW V,
    NEW m, DOMAIN m \subseteq K, IsAMap(V, m)
  PROVE
    MapToSet(m) \subseteq K \X V
PROOF
<*> \A k \in DOMAIN m: <<k, m[k]>> \in K \X V
  OBVIOUS
<*> QED
  BY DEF MapToSet


THEOREM
  ASSUME
    NEW K, NEW V,
    NEW s, s \subseteq K \X V
  PROVE
    DOMAIN SetToMap(s) \subseteq K /\ IsAMap(V, SetToMap(s))
PROOF
  BY DEF SetToMap

----

THEOREM
  ASSUME
    NEW K, NEW V
  PROVE
    DOMAIN EmptyMap \subseteq K /\ IsAMap(V, EmptyMap)
PROOF
  BY DEF EmptyMap


THEOREM MapletType ==
  ASSUME
    NEW K, NEW V,
    NEW k \in K, NEW v \in V
  PROVE
    DOMAIN (k :> v) \subseteq K /\ IsAMap(V, k :> v)
PROOF
  BY DEF :>


THEOREM MapOverlayType ==
  ASSUME
    NEW K, NEW V,
    NEW a, DOMAIN a \subseteq K, IsAMap(V, a),
    NEW b, DOMAIN b \subseteq K, IsAMap(V, b)
  PROVE
    DOMAIN (a @@ b) \subseteq K /\ IsAMap(V, a @@ b)
PROOF
  BY DEF @@


THEOREM
  ASSUME
    NEW K, NEW V,
    NEW m, DOMAIN m \subseteq K, IsAMap(V, m), IsFiniteSet(DOMAIN m)
  PROVE
    MapCardinality(m) \in Nat
PROOF
<*> ASSUME NEW x, IsFiniteSet(x) PROVE Cardinality(x) \in Nat
  OMITTED  \* i.e., FS_CardinalityType
<*> QED
\* FIXME: crud, TLAPM gets confused by Cardinality being instance-local in Maps
\* (it doesn't inline-expand it like other locals)
\*  BY DEFS Cardinality, MapCardinality


THEOREM
  ASSUME
    NEW K, NEW V,
    NEW m, DOMAIN m \subseteq K, IsAMap(V, m),
    NEW Test(_), ASSUME NEW k \in K PROVE Test(k) \in BOOLEAN
  PROVE
    DOMAIN SelectMap(m, LAMBDA k: Test(k)) \subseteq K /\ IsAMap(V, SelectMap(m, LAMBDA k: Test(k)))
PROOF
  BY DEF SelectMap


THEOREM MapSubtractType ==
  ASSUME
    NEW K, NEW V,
    NEW m, DOMAIN m \subseteq K, IsAMap(V, m),
    NEW k
  PROVE
    DOMAIN (m -- k) \subseteq K /\ IsAMap(V, m -- k)
PROOF
  BY DEF --

====
