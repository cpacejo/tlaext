---- MODULE Variants ----

(*
A standard tagged variant type, implemented as a function of a single value
(the tag).  Record syntax may thus be used to construct and destruct
variants.
*)

Var(T) == UNION { [ { t } -> T[t] ]: t \in DOMAIN T }

VarTag(v) == CHOOSE t \in DOMAIN v: TRUE
VarValue(v) == v[VarTag(v)]

====
